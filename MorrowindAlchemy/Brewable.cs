﻿using System.Collections.Generic;

namespace MorrowindAlchemy
{
    class Brewable
    {
        public AlchemyItem Item1 { get; set; }
        public AlchemyItem Item2 { get; set; }
        public IEnumerable<string> Effects { get; set; }
    }
}
