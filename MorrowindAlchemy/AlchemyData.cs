﻿using System.Collections.Generic;

namespace MorrowindAlchemy
{
    class AlchemyData
    {
        public IEnumerable<AlchemyItem> Ingredients { get; set; }
        public IEnumerable<AlchemyItem> Effects { get; set; }
    }

    class AlchemyItem
    {
        public string Name { get; set; }
        public IEnumerable<string> Properties { get; set; }
        public IEnumerable<Vendor> Vendors { get; set; }
        public bool ShowVendors { get; set; }
        public bool InInventory { get; set; }
    }

    class Vendor
    {
        public string Name { get; set; }
        public string Location { get; set; }
    }
}
